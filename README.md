# Vue.js application development

A Tic Tac Toe game.

## References

- Official Website: https://vuejs.org/
- Devtools for Chrome: https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbp
- Application example -> Twitter live stream: https://github.com/fabiandev/vue-twitter-stream-app
- Bulma (modern CSS framework based on Flexbox): https://bulma.io/
